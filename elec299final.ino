#include <Servo.h> 

const int bumperL = 13;
const int bumperR = 12;
const int E1 = 6;
const int M1 = 7;
const int E2 = 5;
const int M2 = 4;
char *message = new char[2];
boolean sendReady = true;

#define pitchUP 160
#define pitchDOWN 60
#define gripCLOSE 110
#define gripOPEN 30

#define rSPEED 200
#define lSPEED 190
#define leftMOD 20

#define turnTIME 250
#define driveTIME_rBall 30
#define driveTIME_rCenter 300
#define driveTIME_fCenter 250

#define WHITE 550

#define panA 90
#define panB 90
#define panC 88

enum {RIGHT,LEFT};
enum {BLACK=2,BUMP};

Servo pan, pitch, grip; 

void setup(){  
  pinMode(bumperL,INPUT);
  pinMode(bumperR,INPUT);
  pinMode(A0,INPUT);  
  pinMode(A5,INPUT); 
  pinMode(A4,INPUT); 
  pinMode(A3,INPUT);   
  
  pan.attach(2); 
  pitch.attach(3); 
  grip.attach(8);
  
  //pitch.write(120);
  pan.write(90);
  grip.write(gripOPEN); //185 MAX CLOSE
  
  //Gives time to set servos

  //BLUETOOTH COMM PORT
  Serial.begin(9600);
  
  digitalWrite(M1,HIGH);
  digitalWrite(M2,HIGH);
}
//M1 right
//M2 left
void loop(){
  //wait for bluetooth valid connection
  if (sendReady){
     Serial.println("30");
     sendReady = false;
  }
  if(Serial.available())
  {
     sendReady = true;
     //read sent byte
     char inByte = Serial.read();
     
     switch (inByte){
     case 'A': //right
        message[0] = '0';
        message[1] = 'A';
        Serial.println(message);
        
        pan.write(panA);
        
        turnRight(turnTIME);        
        //rotate towards ball
        rotatetoBlack(RIGHT);        
        //go to ball, grab and return!
        toBallandBack();  
        //rotate left to base
        turnLeft(turnTIME);
        rotatetoBlack(LEFT);        
        toBaseandBack();       
        break;
        
     case 'B': //forward
        message[0] = '0';
        message[1] = 'B';
        Serial.println(message);
        
        pan.write(panB);
        forward(0);
        toBallandBack();
        toBaseandBack();       
        break;
        
     case 'C': //left
        message[0] = '0';
        message[1] = 'C';
        Serial.println(message);
        
        pan.write(panC);
        turnLeft(turnTIME);
        //rotate towards ball
        rotatetoBlack(LEFT);
        //go to ball, grab and return!
        toBallandBack();        
        //rotate right to base
        turnRight(turnTIME);
        rotatetoBlack(RIGHT);        
        toBaseandBack(); 
        break;
     default:
        //just skip...
        break;
     }    
  }

    Serial.println(digitalRead(bumperL));
    Serial.println(digitalRead(bumperR));
    delay(500);

}

//follow line function!
//2 modes (int mode)
//  1: drive to black;
//  2: drive to bump - realign
//2 out comes
//  0: stop condition
//  1: follow condition
//TESTED + WORKING
int linefollow(int mode){ 
  int left = analogRead(A3);
  int mid = analogRead(A4);
  int right = analogRead(A5);
  
  //drive to black mode
  if (mode == BLACK){
    //solid line detected
    if(((left)<WHITE)&&((mid)<WHITE)&&((right)<WHITE)){ 
      //stop condition
      return 0;
    }
  }
  //drive to bump mode
  else if (mode == BUMP)
  {
    if (((digitalRead(bumperL)== HIGH)||(digitalRead(bumperR) == LOW))){
      return 0; //stop condiation    
    }
    else if ((left<WHITE)&&(mid<WHITE)&&(right<WHITE))
    {
      forward(0);
      //three black detected! Drive until bumpers pressed
      while (!((digitalRead(bumperL)== HIGH)||(digitalRead(bumperR) == LOW))){}
      return 0;
    }
  }       
  //middle on black
  if(mid<WHITE){
    analogWrite(E1, rSPEED);
    analogWrite(E2, lSPEED);
  }
  else if((left-right)>40){
    analogWrite(E2, int(lSPEED/2));
    while((analogRead(A4))>WHITE){}
    analogWrite(E2, lSPEED);
  }
  else if((right-left)>40){
    analogWrite(E1, int(rSPEED/20));
    while((analogRead(A4))>WHITE){}
    analogWrite(E1, rSPEED);
  }
  delay(20);
  return 1;
}


void grab(){  
  STOP();
  delay(200);
  pitch.write(pitchDOWN);
  delay(300);
  grip.write(gripCLOSE);
  delay(200);
  pitch.write(pitchUP); 
}

void drop(){
  STOP();
  pitch.write(pitchDOWN+70);
  delay(400);
  grip.write(gripOPEN);
  delay(100);
  //pitch.write(pitchUP);
}

//rotate to blackline in given direction
void rotatetoBlack(int dir)
{
   digitalWrite(M1, dir);
   digitalWrite(M2,!dir);
   analogWrite(E1,rSPEED);
   analogWrite(E2,lSPEED);
   
   //keep spinnin
   while(analogRead(A4)>WHITE&&analogRead(A3)>WHITE&&analogRead(A5)>WHITE){}
   
  digitalWrite(M1,!dir);
  digitalWrite(M2,dir);
   
   analogWrite(E1,rSPEED);
   analogWrite(E2,lSPEED);
   delay(50);
   STOP();
   forward(0);  
}

//rotate left full speed for given time
void turnLeft(int time)
{
        digitalWrite(M1,HIGH);
        digitalWrite(M2,LOW);
        analogWrite(E1,rSPEED);
        analogWrite(E2,lSPEED);
        delay(time);
}

//rotate right full speed for given time
void turnRight(int time)
{
        digitalWrite(M1,LOW);
        digitalWrite(M2,HIGH);
        analogWrite(E1,rSPEED);
        analogWrite(E2,lSPEED);
        delay(time);
}

//Drive forward full teams.
void forward(int time)
{
   digitalWrite(M1,HIGH);
   digitalWrite(M2,HIGH);
   analogWrite(E1,rSPEED);
   analogWrite(E2,lSPEED);
   delay(time); 
}

//STOP motors.
void STOP()
{
   digitalWrite(M1,LOW);
   digitalWrite(M2,LOW);
   delay(50);
   analogWrite(E1,0);
   analogWrite(E2,0);
   digitalWrite(M1, HIGH);
   digitalWrite(M2,HIGH);
   
}

//Function: Line follow to base, drop ball, return to center.
void toBaseandBack()
{
  
    forward(0);
    
    //drive to base (wait until 1 bumps
    while(linefollow(BUMP)){}  
    pan.write(80);
    //stop, and drop ball
    drop();
    
    message[0] = '2';
    Serial.println(message);
    
    //return to middle
    reverse(driveTIME_rCenter);
    turnLeft(turnTIME);
    rotatetoBlack(LEFT);
    while(linefollow(BLACK)){} 
    
    STOP();   
    forward(driveTIME_fCenter);
    STOP();

}

//Function: Line follow to ball, grab ball, return to center. 
void toBallandBack()
{
       //follow black line to ball (stop on bump)
        forward(0);
        
        while(linefollow(BUMP)){}       
      
        reverse(driveTIME_rBall);
        //grab ball
        grab();
        
        message[0] = '1';
        Serial.println(message);
        
        reverse(driveTIME_rCenter);
        //turn around
        turnRight(turnTIME);
        rotatetoBlack(RIGHT);
        //drive back to center(stop on double black) + driveTIME_fCenter
        while(linefollow(BLACK)){}    
        STOP();   
        forward(driveTIME_fCenter);
        STOP();

}

void reverse(int time)
{
   digitalWrite(M1,LOW);
   digitalWrite(M2,LOW);
   analogWrite(E1,rSPEED);
   analogWrite(E2,lSPEED);
   delay(time); 
}

